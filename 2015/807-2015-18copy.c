#include <stdio.h>
#include <string.h>
char *fun(char *str,int m){
    int i,j,n=strlen(str);
    char nstr[100];
    // 将str的前m位添加给新数组nstr
    strncpy(nstr,str,m);
    // 将str和nstr合并
    strcat(str,nstr);
    //添加结束符号
    str[n+m] = '\0';
    // 每次整体向前移动一位。一共移动m次
     for(i=0;i<m;i++){ 
        for(j=0;j<strlen(str);j++){
            str[j] = str[j+1];
        }
    }
    
    return str;
}
int main(){
   char s[100];
   int m;
   puts("请输入一个字符串：\n");
   scanf("%s",s);
   puts("请输入要移动几位：\n");
   scanf("%d",&m);
   printf("%s\n",fun(s,m));
   return 0; 
}