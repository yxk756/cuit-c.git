#include <stdio.h>
int main()
{
	int i,j,k,temp;
	scanf("%d%d%d",&i,&j,&k);
	if(i>j){  //i比j大，交换位置，保证从小到大的排列
		temp = j;
		j = i;
		i = temp;
	}
	if(i>k){  //i比k大，交换位置，保证从小到大的排列
		temp = i;
		i = k;
		k = temp;
	}
	if(j>k){ //j比k大，交换位置，保证从小到大的排列
		temp = j;
		j = k;
		k = temp;
	}
	printf("%3d%3d%3d",i,j,k);
	return 0;
 } 
