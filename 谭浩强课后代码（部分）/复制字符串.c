#include <stdio.h>
#include <string.h>
/**
复制字符串 a b,
a == b 0
a > b  正数  第一个不相等的ASCII差值
a < b  负数  第一个不相等的ASCII差值
 */
int main(){
    char s1[20],s2[20],s3[20];
    int i = 0,result;
    printf("Input String A：\n");
    scanf("%s",s1);
    while (s1[i] != '\0'){
        s2[i] = s1[i];
        i++;
    }
    s2[i] = '\0';
    strcpy(s3,s1);
    printf("自定义复制结果:%s\n",s2);
    printf("strcpy复制结果:%s\n",s3);
    return 0;
}