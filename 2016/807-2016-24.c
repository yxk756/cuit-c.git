#include <stdio.h>
int main(){
    int score;
    char grade;
    printf("请输入成绩:");
    scanf("%d",&score);
    if(score < 0 || score > 100){
        printf("Error Input！");
        return 0;  //退出程序
    }
    switch (score / 10) //将成绩转成一位数，例如 74 / 10 = 7
    {
    case 9: grade = 'A'; break;
    case 8: grade = 'B'; break;
    case 7: grade = 'C'; break;
    case 6: grade = 'D'; break;
    default: grade = 'E';
    }
    // 输出
    printf("分数为%d,等级为:%c",score,grade);
    return 0;
}