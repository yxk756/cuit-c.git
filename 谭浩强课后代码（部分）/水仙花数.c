#include <stdio.h>
#define N 1000
/**
 *  水仙花是一个3位数 
 *  153是水仙花数，153 = 1^3 + 5^3 + 3^3
 *  完数等于因子之和 例如 6 = 1 + 2 + 3
 */
int main(){
    int i,j,k,l,m;  // k个位数   l十位数   m百位数
    for(i=100;i<1000;i++){
        k = i % 10;  //个位数 
        l = i / 10 % 10;  //十位数
        m = i / 100;  //百位数
        if(i == k*k*k + l*l*l + m*m*m){
            printf("%d是水仙花数\n",i);
        }
    }
    return 0;
}