#include <stdio.h>
#define M 4
#define N 5
int fun(int a[M][N],int m,int n){
    int i,j,sum = 0;
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            if(i == 0 || i == m-1){  //第一行和最后一行的元素求和
                sum = sum + a[i][j];
            }else if(j == 0 || j == n-1){  // 第一列和最后一列元素（除了第一行和最后一行）求和
                sum = sum + a[i][j];
            }
        }
    }
    return sum;
}
int main(){
    int a[M][N],i,j;
    printf("请输入%dX%d大小的数组：",M,N);
     for(i=0;i<M;i++){  //输入二维数组
        for(j=0;j<N;j++){
            scanf("%d",&a[i][j]);
        }
    }
    printf("数组的周边元素之和为：%d",fun(a,M,N));
    return 0;
}