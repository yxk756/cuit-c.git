#include <stdio.h>
#define M 4
/**
1 2 3           1 4 7
4 5 6   =====>  2 5 8
7 8 9           3 6 9
 */
int main(){
    int arry[M][M],i,j,temp;
    printf("输入%d阶矩阵：\n",M);
    for(i=0;i<M;i++){
        for(j=0;j<M;j++){
         scanf("%d",&arry[i][j]);
        }  
    }
    printf("original:\n");
    for(i=0;i<M;i++){
        for(j=0;j<M;j++){
         printf("%d ",arry[i][j]);
        }  
        printf("\n");
    }
      for(i=0;i<M;i++){
        for(j=i+1;j<M;j++){
         temp = arry[i][j];
         arry[i][j] = arry[j][i];
         arry[j][i] = temp;
        }  
    }
    printf("Now:\n");
    for(i=0;i<M;i++){
        for(j=0;j<M;j++){
         printf("%d ",arry[i][j]);
        }  
        printf("\n");
    }
    return 0;
}