#include <stdio.h>
#include <string.h>
int main(){
    int i,eng = 0,speace = 0,num = 0,other = 0;
    char s[30];
    gets(s); //获取用户输入的字符串
    for(i=0;i<strlen(s);i++){  
        if( (s[i] >= 'A' && s[i] <= 'Z') || (s[i] >= 'a' && s[i] <= 'z')) eng++; //统计英文字母
        else if(s[i] >= '0' && s[i] <= '9') num++; //统计数字
        else if(s[i] == ' ') speace++; //统计空格
        else other++; //统计其他字符
    }
    // 输出
    printf("英文字母有%d个,空格有%d个,数字有%d个,其他字符有%d个",eng,speace,num,other);
    return 0;
}