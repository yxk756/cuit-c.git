#include <stdio.h>
#include <stdlib.h>
#define N 3
struct student{
	int id; //学号
	char name[10]; //姓名
	float scores[3];//三门课成绩
	float ave_socre; //平均分 
};
void Input(struct student s[],int n)
{
	int i;
	printf("请输入学生信息，如：2018 张三 95 97 99\n");
	for(i=0;i<n;i++)
	{
		scanf("%d %s %f %f %f",&s[i].id,s[i].name,&s[i].scores[0],&s[i].scores[1],&s[i].scores[2]);
		s[i].ave_socre = (s[i].scores[0]+s[i].scores[1]+s[i].scores[2]) / 3; //计算平均成绩 
	}
}
void Output(struct student s[],int n)
{
	int i;
	printf("%10s%10s%10s%10s%10s%10s\n","学号","姓名","科目一","科目二","科目三","平均分");
	for(i=0;i<n;i++)
	{
	printf("%10d%10s%10.2f%10.2f%10.2f%10.2f\n",s[i].id,s[i].name,s[i].scores[0],s[i].scores[1],s[i].scores[2],s[i].ave_socre);
	}
}
int main()
{
//	struct student *s = (struct student*) malloc(N*sizeof(struct student));
	int i;
	struct student s[N];
	for(i=0;i<N;i++)
	{
		s[i] = *(struct student*) malloc(N*sizeof(struct student));	
	}
	
	Input(s,N);
	Output(s,N);
	return 0;
}
