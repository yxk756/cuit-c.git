#include <stdio.h>
#define N 100
struct people{
    char name[10]; //姓名
    int gender; //性别 1是男  0是女
    int age;  //年龄
}p[N];
int main(){
    //man[0]存0-29岁、man[1]存30-59、man[2]存60以上人数，woman存女的人数
    int i,man[3]={0},woman[3]={0}; 
    for(i=0;i<N;i++){
        scanf("%s%d%d",p[i].name,&p[i].gender,&p[i].age);
    }
    for(i=0;i<N;i++){
        if(p[i].gender == 0){ //统计女生
            if(p[i].age>=0 && p[i].age<30){
                woman[0]++;
            }
            if(p[i].age>=30 && p[i].age<59){
                woman[1]++;
            }
            if(p[i].age>=60){
                woman[2]++;
            }
        }else{ //统计男生
        if(p[i].age>=0 && p[i].age<30){
                man[0]++;
            }
            if(p[i].age>=30 && p[i].age<59){
                man[1]++;
            }
            if(p[i].age>=60){
                man[2]++;
            }
        }
    }
   //输出
    printf("男生： 0-29岁：%d个，30-59岁：%d个，60岁以上：%d个\n",man[0],man[1],man[2]);
    printf("女生： 0-29岁：%d个，30-59岁：%d个，60岁以上：%d个\n",woman[0],woman[1],woman[2]);
    return 0;
}