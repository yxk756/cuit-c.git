#include <stdio.h>
#define N 100
int main(){
    //申明文件指针
    FILE *fp;
    //打开文件 w+ 写
    fp = fopen("file.txt","w+");
    char c;
    // 如果不是#就将 输入的字符写入到文件 否则结束循环
    while ((c = getchar()) != '#')
    {
       fputc(c,fp); 
    }
    // 关闭文件流
    fclose(fp);
    
   return 0; 
}