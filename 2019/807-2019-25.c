#include <stdio.h>
#define N 3
struct student{
    int id; //学号
    int math; //数学成绩
    int C; //C语言成绩
} students[N],max_stu[N];  //students记录所有学生信息，max_stu记录C语言成绩最高的学生记录。
int main(){
    int i,j = 0,max = 0;
    printf("请输入%d个学生的信息：\n",N);
    for(i=0;i<N;i++){
        scanf("%d%d%d",&students[i].id,&students[i].math,&students[i].C);
    }
    //找到最高分
    for(i=0;i<N;i++){
        if(max < students[i].C){
            max = students[i].C;
        }
    }
    //找到最高分对应的学生，加入到数组中去，并统计人数
    for(i=0;i<N;i++){
        if(max == students[i].C){
            max_stu[j] = students[i];
            j++;
        }
    }
    //输出
    printf("C语言成绩最高的有%d个\n",j+1);
    for(i=0;i<=j;i++){
       printf("学号:%d,数学:%d,C语言:%d\n",max_stu[i].id,max_stu[i].math,max_stu[i].C);
    }
    return 0;
}