#include <stdio.h>
#include <string.h>
char *fun(char *str,int m){
    int i,j,n = strlen(str);
    for(i=0;i<m;i++){
        // 先把前面的m个元素插入到表后面
        str[n+i] = str[i];  
    }
    //添加完毕后再末尾添加 \0 来表示结束
    str[n+m] = '\0';
    // 每次整体向前移动一位。一共移动m次
     for(i=0;i<m;i++){ 
        for(j=0;j<strlen(str);j++){
            str[j] = str[j+1];
        }
    }
    return str;
}
int main(){
   char s[100];
   int m;
   puts("请输入一个字符串：\n");
   scanf("%s",s);
   puts("请输入要移动几位：\n");
   scanf("%d",&m);
   printf("%s\n",fun(s,m));
   return 0; 
}