#include <stdio.h>
#define N 20
float fmax(float s[N])
{
	int i;
	float max;
	max = s[0]; //假设第一个为最大 
	for(i=0;i<N;i++)
	{
		if(max<s[i]) max = s[i];  //当前max小于其中的元素，max重新赋值 
	}
	return max;
}
float faver(float s[N])
{
	float ave = 0;
	int i;
	for(i=0;i<N;i++)
	{
		ave += s[i];  //算出总数 
	}
	ave = ave / N; //总数除以平均数 
	return ave;
}
void fcount(float s[N])
{
	int i,a,b,c,d;
	a = 0;
	b = 0;
	c = 0;
	d = 0;
	for(i=0;i<N;i++)
	{
		if(s[i]<=100 && s[i]>=90) a++;  //90 -100 A
		if(s[i]<90 && s[i]>=80) b++; // 90 -89 B 
		if(s[i]<80 && s[i]>=60) c++; // 60-79 C
		if(s[i]<60 && s[i]>=0) d++; // 60以下 D 
	}
	printf("A:%d人,B:%d人,C:%d人,D:%d人\n",a,b,c,d);
}
int main()
{
	float s[N],max,ave;
	int i;
	for(i=0;i<N;i++) scanf("%f",&s[i]);  //输入20个学生成绩  【千万注意这里有&】 
	max = fmax(s);
	ave = faver(s);
	fcount(s);
	printf("max=%f,ave=%f\n",max,ave);
	return 0;
 } 
