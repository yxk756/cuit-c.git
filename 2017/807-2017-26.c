#include <stdio.h>
#define N 2
typedef struct student{ //定义结构体
    char name[10];
    float lang;
    float mat;
    float eng;
} student;  //重命名为student
int main(){
    int i,index=0;
    float ave,max = 0;
    student s[N]; //定义结构体数组
    printf("请输入学生姓名和成绩：");
    for(i=0;i<N;i++){  //循环给结构体数组赋值
        scanf("%s%f%f%f",s[i].name,&s[i].lang,&s[i].mat,&s[i].eng);
    }
    for(i=0;i<N;i++){
       ave = (s[i].lang + s[i].mat + s[i].eng) / 3;  //算出每个人平均分
       if(ave > max){  //若大于最高分
           max = ave; //重新赋值最大值
           index = i; //记录下标
       }
    }
    // 输出
    printf("name: %s, 语文: %2.2f, 数学: %2.2f, 外语：%2.2f,平均分：%2.2f",s[index].name,s[index].lang,s[index].mat,s[index].eng,max);
    return 0;
}