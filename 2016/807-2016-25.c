#include <stdio.h>
#include <string.h>
int main(){
    int i,n;
    char s[30];
    gets(s);
    n = strlen(s);  //获取字符串长度
    for(i=0;i< n/2;i++){  // 循环数组的一半即可
        if(s[i] != s[n-i-1]){  // 数组首位是否一致
            printf("No");   // 不一致 打印No ，结束程序
            return 0;
        }
    }
    printf("Yes");  //正常退出程序，说明是回文数
    return 0;
}