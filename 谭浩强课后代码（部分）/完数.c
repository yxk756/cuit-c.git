#include <stdio.h>
#define N 1000
/**
 *  找出1000以内所有完数
 *  完数等于因子之和 例如 6 = 1 + 2 + 3
 */
int main(){
    int i,s,j;
    for(i=2;i<N;i++){
        s = 0;
        for(j=1;j<i;j++) //判断j是不是因子 是的话就把他们加起来
        if(i%j == 0)    s+=j;
        if(s == i){  //因子和等于该数，是完数
            printf("%d是完数,它的因子是: ",i);
            for(j=1;j<i;j++){
                if(i%j == 0){
                    printf("%d ",j);
                }
            }
            printf("\n");
        }
    }
    return 0;
}